=== Openki ===
Contributors: openki
Donate link: https://about.openki.net/en/ueber-uns/spenden/
Tags: education, selforganization, lerning, p2p, grassroot, calendar, propose, collective, share, unconference, barcamp, openspace, conference, schedule
Tested up to: 5.7.2
Stable tag: 1.0.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0

Show courses and the event calendar from Openki.net received through the JSON-API.

== Description ==

This plugin can display courses and events in different views. You can customize the style flexibly with simple css. The plugin supports  and so on.

= Docs and support =

There is not yet a [documentation](https://gitlab.com/Openki/wordpress-plugin/-/wikis/home) but you can find technical details about the API in our [wiki](https://gitlab.com/Openki/Openki/-/wikis/JSON-API) and more detailed information about the project Openki on [about.openki.net](https://about.openki.net/). When you can't find the answer to your question in any of the documentation, write us to our suport email: support at openki.net or create a new ticket in our [issue tracker](https://gitlab.com/Openki/wordpress-plugin/-/issues).

= Openki needs your support =

It is hard to continue development and support for this free plugin without contributions from users like you. If you enjoy using Openki and find it useful, please consider [making a donation](https://about.openki.net/en/ueber-uns/spenden/). Your donation will help encourage and support the plugin's continued development and better user support.

= Privacy notices =

With the default configuration, this plugin, in itself, does not:

* track users by stealth;
* write any user personal data to the database;
* send any data to external servers;
* use cookies.

= Translations =

You can translate this plugin in the [source code](https://gitlab.com/Openki/wordpress-plugin/)

== Installation ==

1. Upload the entire `Openki` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the **Plugins** screen (**Plugins > Installed Plugins**).

You will find **Openki** menu in your WordPress admin screen.
