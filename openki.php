<?php

/**
 * @package Openki
 */
/*
Plugin Name: Openki
Plugin URI: https://openki.net/
Description: Show courses and the event calendar from Openki.net received through the JSON-API.
Version: 1.0.0
Author: Verein KOPF
Author URI: https://about.openki.net/
License: GPLv3
*/

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define('OPENKI__PLUGIN_VERSION', '1.0.0');
define('OPENKI__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('OPENKI__PLUGIN_URL', plugin_dir_url(__FILE__));
define('OPENKI__API', 'https://dev.openki.net/api/0/json/');

require_once(OPENKI__PLUGIN_DIR . './shortcodes/events.php');
require_once(OPENKI__PLUGIN_DIR . './shortcodes/courses.php');

// add shortcode
function openki_shortcodes_init()
{
	add_shortcode('openki_events', 'openki_events');
	add_shortcode('openki_courses', 'openki_courses');
}

add_action('init', 'openki_shortcodes_init');

// add styles and scripts
function openki_enqueue_style()
{
	wp_enqueue_style('openki-theme', OPENKI__PLUGIN_URL . 'frontend/style.css', array(), OPENKI__PLUGIN_VERSION);
}

function openki_enqueue_script()
{
	wp_enqueue_script('openki-js', OPENKI__PLUGIN_URL . 'frontend/openki.js', array('jquery'), OPENKI__PLUGIN_VERSION);
}

add_action('wp_enqueue_scripts', 'openki_enqueue_style');
add_action('wp_enqueue_scripts', 'openki_enqueue_script');
