<?php

/**
 * Displays events as a list
 */

function openki_events($attr)
{
	$attributes = array(
		'course' => '',
		'region' => '',
		'search' => '',
		'categories' => '',
		'group' => '',
		'groups' => '',
		'venue' => '',
		'room' => '',
		'start' => '',
		'before' => '',
		'after' => 'now',
		'end' => '',
		'internal' => '',
		'skip' => '0',
		'limit' => '15',
		'display' => 'calendar', // custom attribute to define display type
		'link_icon' => OPENKI__PLUGIN_URL . 'frontend/icons/link.svg', // custom attribute to change link icon
		'link_text' => 'Diesen Event auf Openki anzeigen',
		'all_link' => 'https://openki.net', // custom attribute for the link after the list
		'all_link_text' => 'Alle Events auf Openki anzeigen', // custom attribute for the link text after the list
		'color_font' => 'black',
		'color_background' => 'rgba(0, 0, 0, 0.1)',
		'color_background_hover' => 'rgba(0, 0, 0, 0.2)',
	);

	$a = shortcode_atts($attributes, $attr);

	// remove custom attributes from API
	$display = esc_attr($a['display']);
	unset($a['display']);
	$linkIcon = esc_attr($a['link_icon']);
	unset($a['link_icon']);
	$linkText = esc_attr($a['link_text']);
	unset($a['link_text']);
	$allLink = esc_attr($a['all_link']);
	unset($a['all_link']);
	$allLinkText = esc_attr($a['all_link_text']);
	unset($a['all_link_text']);

	$colorFont = esc_attr($a['color_font']);
	unset($a['color_font']);
	$colorBackground = esc_attr($a['color_background']);
	unset($a['color_background']);
	$colorBackgroundHover = esc_attr($a['color_background_hover']);
	unset($a['color_background_hover']);

	$query = http_build_query(array_filter($a));
	$options = base64_encode('{"display": "' . $display . '", "linkIcon": "' . $linkIcon . '", "linkText": "' . $linkText . '", "allLink": "' . $allLink . '", "allLinkText": "' . $allLinkText . '"}');
	$styles = '--openki--color-font: ' . $colorFont . '; --openki--color-background: ' . $colorBackground . '; --openki--color-background-hover: ' . $colorBackgroundHover . ';';

	$output = '<div class="openki openki-events" data-openki-url="' . esc_attr(OPENKI__API) . 'events/?' . esc_attr($query) . '" data-openki-options="' . $options . '" style="' . esc_attr($styles) . '"></div>';

	return $output;
}
