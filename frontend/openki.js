/**
 * Loads data from the Openki JSON API
 */
 jQuery(function ($) {
	/**
	 * Calls the API and passes the output to the callback function
	 * @param {String} url 
	 * @param {Function} callback 
	 */
	function APICall(url, callback) {
		$.getJSON(url, function (data) {
			if (data.data.length === 0) {
				// TODO: show "no results" text
				console.error('HELP! No results!');
				return;
			}

			callback(data.data);
		}).fail(function (data) {
			console.error(data.data);
		});
	}

	/**
	 * Groups elements by date for the calendar
	 * @param {Array} data 
	 * @returns Array
	 */
	function groupByDate(data) {
		var finalObj = {};

		data.forEach(function (event) {
			var day = event.start.split('T')[0];

			if (finalObj[day]) {
				finalObj[day].push(event);
			} else {
				finalObj[day] = [event];
			}
		})

		return finalObj
	}

	/**
	 * Encodes HTML characters
	 * @param {String} text 
	 * @returns String
	 */
	const $encodeDiv = $('<div/>');
	function htmlEncode(text) {
		return $encodeDiv.text(text).html();
	}

	// iterate over openki elements
	$('.openki-courses').each(function () {
		var wrapper = this;

		APICall(wrapper.dataset.openkiUrl, function (data) {
			var items = [];
			var wrapperClass = '';
			var options = JSON.parse(atob(wrapper.dataset.openkiOptions)); // decode the options passed as base64 encode
			var linkIcon = htmlEncode(options.linkIcon);

			// create calendar display
			wrapperClass = 'openki-calendar';

			data.forEach(function (course) {
				// create each course code

				console.log(course)

				items.push('<li class="openki-calendar-event">');

				if (options.display === 'compact') {
					items.push('<button type="button" class="openki-calendar-event__header openki-calendar-event__header--compact">');
					items.push('<span class="openki-calendar-event__header-date">' + course.region.name + '</span>');
					items.push(htmlEncode(course.name));
					items.push('<span class="openki-calendar-event__header-icon openki-calendar-event__header-icon--toggle"></span>');
					items.push('</button>');
				} else {
					items.push('<a href="' + htmlEncode(course.link) + '" target="_blank" class="openki-calendar-event__header">' + htmlEncode(course.name) + '<span class="openki-calendar-event__header-icon" style="background-image: url(' + linkIcon + ');"></span></a>');
				}

				// region
				if (options.display !== 'compact') {
					items.push('<div class="openki-calendar-event__content">');
					items.push('<div class="openki-calendar-event__row"><div class="openki-calendar-event__label">Region:</div><div class="openki-calendar-event__value">' + course.region.name + '</div></div>');
				} else {
					items.push('<div class="openki-calendar-event__content openki-calendar-event__content--hide">');
				}

				// groups
				if (course.groups.length !== 0) {
					items.push('<div class="openki-calendar-event__row"><div class="openki-calendar-event__label">Gruppen:</div><div class="openki-calendar-event__value">');

					course.groups.map(function (group) {
						items.push('<a href="' + htmlEncode(group.link) + '" target="_blank">' + htmlEncode(group.name) + '</a>');
					})

					items.push('</div></div>');
				}

				var descriptionTooLong = options.display !== 'compact' && course.description.length > 100;
				items.push('<div class="openki-calendar-event__description">');
				if (descriptionTooLong) {
					items.push('<a href="#" class="openki-calendar-event__description-button">Mehr anzeigen</a>');
				}
				items.push('<div class="openki-calendar-event__description-inner ' + (!descriptionTooLong ? 'openki-calendar-event__description-inner--show' : '') + '">');
				items.push(course.description);
				items.push('<div class="openki-events__link-wrapper openki-events__link-wrapper--event">');
				items.push('<span class="openki-events__link-icon" style="background-image: url(' + linkIcon + ');"></span>');
				items.push('<a href="' + htmlEncode(course.link) + '" target="_blank" class="openki-events__link">' + htmlEncode(options.linkText) + '</a>');
				items.push('</div></div>');

				items.push('</div>'); // close description

				items.push('</div>'); // close event content

				items.push('</li>'); // close event

				/*
				items.push('<li class="openki-calendar-event">');
				if (options.display === 'compact') {
					items.push('<button type="button" class="openki-calendar-event__header openki-calendar-event__header--compact">');
					items.push('<span class="openki-calendar-event__header-date">' + eventDate.join('') + '</span>');
					items.push(htmlEncode(event.title));
					items.push('<span class="openki-calendar-event__header-icon openki-calendar-event__header-icon--toggle"></span>');
					items.push('</button>');
				} else {
					items.push('<a href="' + htmlEncode(event.link) + '" target="_blank" class="openki-calendar-event__header">' + htmlEncode(event.title) + '<span class="openki-calendar-event__header-icon" style="background-image: url(' + linkIcon + ');"></span></a>');
				}

				// when
				if (options.display !== 'compact') {
					items.push('<div class="openki-calendar-event__content">');
					items.push('<div class="openki-calendar-event__row"><div class="openki-calendar-event__label">Wann?</div><div class="openki-calendar-event__value">' + eventDate.join('') + '</div></div>');
				} else {
					items.push('<div class="openki-calendar-event__content openki-calendar-event__content--hide">');
				}

				// where
				if (event.venue && event.venue.name) {
					items.push('<div class="openki-calendar-event__row"><div class="openki-calendar-event__label">Wo?</div><div class="openki-calendar-event__value">');

					if (event.venue.link) {
						items.push('<a href="' + htmlEncode(event.venue.link) + '" target="_blank">' + htmlEncode(event.venue.name) + '</a>');
					} else {
						items.push(htmlEncode(event.venue.name));
					}

					items.push('</div></div>');
				}

				var descriptionTooLong = options.display !== 'compact' && event.description.length > 100;
				items.push('<div class="openki-calendar-event__description">');
				if (descriptionTooLong) {
					items.push('<a href="#" class="openki-calendar-event__description-button">Mehr anzeigen</a>');
				}
				items.push('<div class="openki-calendar-event__description-inner ' + (!descriptionTooLong ? 'openki-calendar-event__description-inner--show' : '') + '">');
				items.push(event.description);
				items.push('<div class="openki-events__link-wrapper openki-events__link-wrapper--event">');
				items.push('<span class="openki-events__link-icon" style="background-image: url(' + linkIcon + ');"></span>');
				items.push('<a href="' + htmlEncode(event.link) + '" target="_blank" class="openki-events__link">' + htmlEncode(options.linkText) + '</a>');
				items.push('</div></div>');

				items.push('</div>'); // close description

				items.push('</div>'); // close event content

				items.push('</li>'); // close event
				*/
			})

			// Openki link
			items.push('<li class="openki-events__link-wrapper">');
			items.push('<span class="openki-events__link-icon" style="background-image: url(' + linkIcon + ');"></span>');
			items.push('<a href="' + htmlEncode(options.allLink) + '" class="openki-events__link">' + htmlEncode(options.allLinkText) + '</a>');
			items.push('</li>');

			$('<ul/>', {
				'class': wrapperClass,
				html: items.join('')
			}).appendTo($(wrapper));
		});
	});

	// iterate over openki elements
	$('.openki-events').each(function () {
		var wrapper = this;

		APICall(wrapper.dataset.openkiUrl, function (data) {
			var items = [];
			var wrapperClass = '';
			var options = JSON.parse(atob(wrapper.dataset.openkiOptions)); // decode the options passed as base64 encode
			var linkIcon = htmlEncode(options.linkIcon);

			// create calendar display
			wrapperClass = 'openki-calendar';

			// group events by date and loop over the days
			const events = groupByDate(data);
			Object.keys(events).forEach(function (day) {
				const displayDate = new Date(day.replace(/-/g, "/")).toLocaleString('de-DE', {
					year: 'numeric', month: 'long', day: 'numeric'
				})

				// create day code
				items.push('<li class="openki-calendar__day-wrapper"><strong class="openki-calendar__day">' + displayDate + '</strong><ul class="openki-calendar__events">');

				// create each event code
				events[day].forEach(function (event) {
					const eventDate = [event.startLocal.split('T')[1]];
					if (event.endLocal.split('T')[1]) eventDate.push(' - ', event.endLocal.split('T')[1]);

					items.push('<li class="openki-calendar-event">');
					if (options.display === 'compact') {
						items.push('<button type="button" class="openki-calendar-event__header openki-calendar-event__header--compact">');
						items.push('<span class="openki-calendar-event__header-date">' + eventDate.join('') + '</span>');
						items.push(htmlEncode(event.title));
						items.push('<span class="openki-calendar-event__header-icon openki-calendar-event__header-icon--toggle"></span>');
						items.push('</button>');
					} else {
						items.push('<a href="' + htmlEncode(event.link) + '" target="_blank" class="openki-calendar-event__header">' + htmlEncode(event.title) + '<span class="openki-calendar-event__header-icon" style="background-image: url(' + linkIcon + ');"></span></a>');
					}

					// when
					if (options.display !== 'compact') {
						items.push('<div class="openki-calendar-event__content">');
						items.push('<div class="openki-calendar-event__row"><div class="openki-calendar-event__label">Wann?</div><div class="openki-calendar-event__value">' + eventDate.join('') + '</div></div>');
					} else {
						items.push('<div class="openki-calendar-event__content openki-calendar-event__content--hide">');
					}

					// where
					if (event.venue && event.venue.name) {
						items.push('<div class="openki-calendar-event__row"><div class="openki-calendar-event__label">Wo?</div><div class="openki-calendar-event__value">');

						if (event.venue.link) {
							items.push('<a href="' + htmlEncode(event.venue.link) + '" target="_blank">' + htmlEncode(event.venue.name) + '</a>');
						} else {
							items.push(htmlEncode(event.venue.name));
						}

						items.push('</div></div>');
					}

					var descriptionTooLong = options.display !== 'compact' && event.description.length > 100;
					items.push('<div class="openki-calendar-event__description">');
					if (descriptionTooLong) {
						items.push('<a href="#" class="openki-calendar-event__description-button">Mehr anzeigen</a>');
					}
					items.push('<div class="openki-calendar-event__description-inner ' + (!descriptionTooLong ? 'openki-calendar-event__description-inner--show' : '') + '">');
					items.push(event.description);
					items.push('<div class="openki-events__link-wrapper openki-events__link-wrapper--event">');
					items.push('<span class="openki-events__link-icon" style="background-image: url(' + linkIcon + ');"></span>');
					items.push('<a href="' + htmlEncode(event.link) + '" target="_blank" class="openki-events__link">' + htmlEncode(options.linkText) + '</a>');
					items.push('</div></div>');

					items.push('</div>'); // close description

					items.push('</div>'); // close event content

					items.push('</li>'); // close event
				})

				items.push('</ul></li>'); // close date
			})

			// Openki link
			items.push('<li class="openki-events__link-wrapper">');
			items.push('<span class="openki-events__link-icon" style="background-image: url(' + linkIcon + ');"></span>');
			items.push('<a href="' + htmlEncode(options.allLink) + '" class="openki-events__link">' + htmlEncode(options.allLinkText) + '</a>');
			items.push('</li>');

			$('<ul/>', {
				'class': wrapperClass,
				html: items.join('')
			}).appendTo($(wrapper));
		});
	});

	/**
	 * Button to show the event description in the default layout
	 */
	$('.openki-events').on('click', '.openki-calendar-event__description-button', function (e) {
		e.preventDefault();

		$(this).addClass('openki-calendar-event__description-button--hide');
		$(this).next().addClass('openki-calendar-event__description-inner--show');
	});

	/**
	 * Button to show the event content in the compact layout
	 */
	$('.openki-events').on('click', '.openki-calendar-event__header--compact', function (e) {
		e.preventDefault();

		$(this).toggleClass('is-active');
		$(this).next().toggleClass('openki-calendar-event__content--hide');
	});
});